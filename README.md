# Status display for miniPiTFT

## Prerequisites

Enable SPI using `sudo raspi-config`.

```shell
sudo pip3 install adafruit-circuitpython-rgb-display
sudo pip3 install --upgrade --force-reinstall spidev
sudo apt-get install python3-pip
sudo apt-get install ttf-dejavu
sudo apt-get install python3-pil
sudo apt-get install python3-numpy
```

## Installation

Add this line to `/etc/rc.local` before the `exit 0`.

```
sudo python3 /home/pi/src/miniPiTFT/stats.py
```
